-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  db5000110738.hosting-data.io
-- Généré le :  Mar 02 Juillet 2019 à 19:23
-- Version du serveur :  5.7.25-log
-- Version de PHP :  7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbs105188`
--

-- --------------------------------------------------------

--
-- Structure de la table `Plantes`
--

CREATE TABLE `Plantes` (
  `Nom_plante` varchar(50) NOT NULL,
  `Temp_opti` int(11) NOT NULL,
  `Humi_opti` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Plantes`
--

INSERT INTO `Plantes` (`Nom_plante`, `Temp_opti`, `Humi_opti`) VALUES
('Rosier', 27, 30),
('Lavande', 29, 25),
('Tulipe', 33, 24),
('Romarin', 32, 17),
('Mimosa', 33, 22),
('Marguerite', 32, 47),
('Orchidée', 33, 44),
('Anémone', 31, 26),
('Basilic', 32, 42),
('Renoncule', 24, 66),
('Fougère', 25, 55),
('Nigritelle', 12, 52);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
